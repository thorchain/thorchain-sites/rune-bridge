import React, { useState, useEffect, useContext } from 'react'
import { Link } from "react-router-dom"
import { Row, Col, Spin } from 'antd';

import Web3 from 'web3'

import Binance from "../../clients/binance"
import { getRUNEBalance, getTokenSupply } from "../../clients/web3"
import axios from 'axios'
import { SYMBOL, BASE_URL, BNB_CONTROLLER_ADDR, ETH_RUNE_ADDR } from '../../env'
import { shortenAddr, getAddrETHLink, getAddrBNBLink, AmounttoString } from '../../utils/utility'

import Breakpoint from 'react-socks';

import { Icon, H1, Text, Button, Center } from '../Components'
import { Context } from '../../context'

const Home = (props) => {

    const context = useContext(Context)

    // const antIcon = <AntIcon type="loading" style={{ fontSize: 24 }} spin />;

    const [bridges, setBridges] = useState({ "BNB": "bnb123..123", "ETH": "0x123...123" })
    const [supplies, setSupplies] = useState({ 'BNB': 0, 'ETH': 0 })
    const [balances, setBalances] = useState({ 'BNB': 0, 'ETH': 0 })
    const [stats, setStats] = useState({ 'total': 0, 'eth_tx': 0, 'bnb_tx': 0, 'total_rune': 0, 'eth_rune': 0, 'bnb_rune': 0 })
    const [error, setError] = useState(false)
    const [available, setAvailable] = useState(true)
    const [loaded, setLoaded] = useState(false)
    const [bridgeError, setBridgeError] = useState(false)


    useEffect(() => {
        getData()
        // getBalances(bridges.ETH)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getData = async () => {
        await getStats()

        let supply_bnb = await getBNBalances(BNB_CONTROLLER_ADDR)
        let supply_eth = await getTokenSupply()
        console.log(supply_eth)
        console.log(supply_bnb)
        let supplies = {
            'BNB': (supply_bnb[0].frozen).toLocaleString(),
            'ETH': (Number(Web3.utils.fromWei(supply_eth))).toLocaleString(),
        }
        setSupplies(supplies)
        console.log(supplies)


        let bridges = await getBridges()
        let balance_eth
        if (bridges) {
            balance_eth = await getRUNEBalance(bridges.ETH)
            let balance_bnb = await getBNBalances(bridges.BNB)
            let balances = {
                'BNB': AmounttoString(Number(balance_bnb[0].free)),
                'ETH': AmounttoString(Number(Web3.utils.fromWei(balance_eth)))
            }
            setBalances(balances)
            console.log(balances)
        } else {
            console.error("no bridges found")
            setError(true)
        }

        setLoaded(true)

        if (+balance_eth <= 10000 * 10 ** 18) {
            console.error("low bridge balance")
            setAvailable(false)
        }

    }

    const getBridges = async () => {
        let bridges;
        try {
            let resp = await axios.get(`${BASE_URL}`)
            //console.log(resp.data.bridges)
            bridges = resp.data.bridges
            setBridges(bridges)
        }
        catch (error) {
            console.error("no bridges")
            setError(true)
        }
        return bridges
    }

    const getBNBalances = async (address) => {
        let relevantBalances = [{
            "icon": "coin-rune",
            "ticker": SYMBOL,
            "free": 0,
            "frozen": 0,
            "locked": 0,
        }]
        try {
            let response = await Binance.getBalances(address)
            //console.log("Balances:", response)
            const b = (response || []).map((bal) => (
                {
                    "icon": bal.symbol === SYMBOL ? "coin-rune" : "coin-bep",
                    "ticker": bal.symbol,
                    "free": parseFloat(bal.free),
                    "frozen": parseFloat(bal.frozen),
                    "locked": parseFloat(bal.locked),
                }
            ))
            relevantBalances = b.filter((x) => x.ticker === SYMBOL)
            //console.log(relevantBalances)
        }
        catch (error) {
            console.error(error)
            return relevantBalances
        }
        return relevantBalances
    }

    const getStats = async () => {
        try {
            let resp = await axios.get(`${BASE_URL}/stats`)
            //console.log(resp.data.jobs)
            if(resp.data.jobs.total_rune === 0){
                console.error("bridge error")
                setError(true)
                setBridgeError(true)
            } else {
                setStats(resp.data.jobs)
            }
            
        }
        catch (error) {
            console.error(error)
        }
        
    }

    const setETH = () => {
        context.setContext({ 'chain': 'ETH' })
    }
    const setBNB = () => {
        context.setContext({ 'chain': 'BNB' })
    }

    const homeStyles = {
        marginLeft: 0,
        marginTop: 40,
        backgroundColor: "#101921"
    }


    const iconStyles = {
        marginTop: 50,
        backgroundColor: "#101921"
    }

    return (
        <div>

            <div style={{ backgroundColor: "#101921" }}>

                <Row style={{}}>
                    <Col xs={24} sm={1} md={2} lg={3}>
                    </Col>

                    <Col xs={24} sm={12} md={12} lg={9} style={homeStyles}>

                        <H1>RUNEBridge</H1>
                        <br></br><br></br>
                        <h4 style={{ color: "#848E9C" }}>
                            <span>SWITCH BETWEEN
                        <span><strong style={{ color: "#fff" }}> BNB.RUNE </strong></span>AND
                        <span><strong style={{ color: "#fff" }}> ETH.RUNE </strong></span>TO ACCESS </span> 
                        <span><strong style={{ color: '#4FE1C4' }}><Link to="/pools"><span role="img" aria-label="sushi">🍣</span> SUSHISWAP</Link></strong></span>
                        </h4><br></br>
                        <h4  style={{ color: "#fff" }}><span role="img" aria-label="sushi">👀</span> BEFORE YOU START: READ THE <a href="https://bitcoin-sage.medium.com/using-the-rune-bridge-to-provide-liquidity-on-sushiswap-4ac852ebfcbe" target="blank">GUIDE</a> BY @BITCOIN_SAGE</h4>
                        <br></br>
                        <Text><span>1) ETH.RUNE is issued and maintained by the THORChain treasury.</span>
                        </Text>
                        <br></br><br></br>
                        <Text><span>2) ETH.RUNE is a transitionary asset deployed to build liquidity ahead of the launch of the Ethereum Bifröst.</span>
                        </Text>
                        <br></br><br></br>
                        <Text><span>3) This bridge is unavoidably centralised, but ETH.RUNE is decentralised and can be used anywhere on Ethereum.</span>
                        </Text>
                        <br></br><br></br>
                        <Text><span>4) On the launch of the Ethereum Bifröst with THORChain, switch from ETH.RUNE to native THOR.RUNE.</span>
                        </Text>
                        <br></br><br></br>
                        <br></br>


                        <br></br>

                    </Col>

                    <Col xs={24} sm={2} md={2} lg={2}>
                    </Col>

                    <Breakpoint medium up>
                        <Col xs={24} sm={8} md={8} lg={9} style={iconStyles}>
                            <Icon icon="rune" style={{ width: 450 }} />
                        </Col>
                    </Breakpoint>

                    <Col xs={24} sm={1} md={2} lg={3}>
                    </Col>

                </Row>
                {!loaded &&
                    <Center>
                        <Spin size="large"></Spin>
                    </Center>
                }
                {loaded &&
                    <>
                        {error &&
                            <>
                                <Row style={{ marginTop: 40 }}>
                                    <Col xs={24} sm={1} md={3} lg={3}>
                                    </Col>
                                    <Col xs={24} sm={12} md={9} lg={9}>
                                        <h4 style={{ color: "#EE5366" }}>BRIDGE OFFLINE TEMPORARILY - DON'T USE</h4>
                                    </Col>
                                    <Col xs={24} sm={1} md={3} lg={3}>
                                    </Col>
                                </Row>
                            </>
                        }

                        {!error &&
                            <>
                                {available &&
                                    <>
                                        <Row style={{ marginTop: 40 }}>
                                            <Col xs={24} sm={1} md={3} lg={3}>
                                            </Col>
                                            <Col xs={24} sm={12} md={9} lg={9}>
                                                <Link to="/wallet/unlock">
                                                    <Button onClick={setBNB} style={{ height: 40, width: 250, margin: 10 }}>SWITCH TO ETH.RUNE &nbsp; &gt; &gt;</Button>
                                                </Link>
                                            </Col>
                                            <Col xs={24} sm={12} md={9} lg={9} style={{ float: 'right', display: 'flex' }}>
                                                <Link to="/wallet/unlock">
                                                    <Button onClick={setETH} style={{ height: 40, width: 250, margin: 10 }}>&lt; &lt; &nbsp; SWITCH TO BNB.RUNE</Button>
                                                </Link>
                                            </Col>
                                            <Col xs={24} sm={1} md={3} lg={3}>
                                            </Col>
                                        </Row>
                                    </>

                                }
                                <Row>
                                    <Row style={{ marginTop: 80 }}>
                                        <Col xs={24} sm={1} md={3} lg={3}>
                                        </Col>
                                        <Col xs={24} sm={24} md={18} lg={18} >
                                            <Row>

                                                <Col xs={24} sm={24} lg={12} style={{ marginTop: 40 }}>
                                                    <strong>SOLVENCY CHECK</strong>
                                                    <br />
                                                    <Text>The assets held on the bridge.</Text>
                                                    <br /><br />
                                                    <Row>
                                                        <Col xs={24} sm={12}>
                                                            <h4 style={{ color: "#848E9C" }}>BNB.RUNE</h4>
                                                            <H1>{balances.BNB}</H1>
                                                            <br />
                                                            <a href={`${getAddrBNBLink(bridges.BNB)}`} target="blank">{shortenAddr(bridges.BNB)}</a>
                                                        </Col>
                                                        <Col xs={24} sm={12}>
                                                            <h4 style={{ color: "#848E9C" }}>ETH.RUNE</h4>
                                                            <H1>{balances.ETH}</H1>
                                                            <br />
                                                            <a href={`${getAddrETHLink(bridges.ETH)}`} target="blank">{shortenAddr(bridges.ETH)}</a>
                                                        </Col>
                                                    </Row>

                                                </Col>

                                            </Row>
                                        </Col>

                                        <Col xs={24} sm={1} md={3} lg={3}>
                                        </Col>

                                    </Row>
                                </Row>

                            </>
                        }

                        <Row>
                            <Row style={{ marginTop: 80 }}>
                                <Col xs={24} sm={1} md={3} lg={3}>
                                </Col>
                                <Col xs={24} sm={24} md={18} lg={18} >
                                    <Row>
                                        <Col xs={24} sm={24} lg={12} style={{ marginTop: 40 }}>
                                            <strong>SUPPLY CHECK</strong>
                                            <br />
                                            <Text>The total circulating supply pegged.</Text>
                                            <br /><br />
                                            <Row>
                                                <Col xs={24} sm={12}>
                                                    <h4 style={{ color: "#848E9C" }}>BNB.RUNE</h4>
                                                    <H1>
                                                        {supplies.BNB}
                                                    </H1>
                                                    <br />
                                                    <a href={`${getAddrBNBLink(BNB_CONTROLLER_ADDR)}`} target="blank">{shortenAddr(BNB_CONTROLLER_ADDR)}</a>
                                                </Col>
                                                <Col xs={24} sm={12}>
                                                    <h4 style={{ color: "#848E9C" }}>ETH.RUNE</h4>
                                                    <H1>{supplies.ETH}</H1>
                                                    <br />
                                                    <a href={`${getAddrETHLink(ETH_RUNE_ADDR)}`} target="blank">{shortenAddr(ETH_RUNE_ADDR)}</a>
                                                </Col>
                                            </Row>

                                        </Col>



                                    </Row>
                                </Col>

                                <Col xs={24} sm={1} md={3} lg={3}>
                                </Col>

                            </Row>
                        </Row>

                        {!bridgeError &&
                        
                        <Row>
                            <Row style={{ marginTop: 80 }}>
                                <Col xs={24} sm={1} md={3} lg={3}>
                                </Col>
                                <Col xs={24} sm={24} md={18} lg={18} >
                                    <Row>
                                        <Col xs={24} sm={24} lg={24} style={{ marginTop: 40 }}>
                                            <strong>VOLUME</strong>
                                            <br />
                                            <Text>The total volume over the bridge.</Text>
                                            <br /><br />
                                            <Row>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>TOTAL (RUNE)</h4>
                                                    <H1>
                                                        {(Number(stats.total_rune)).toLocaleString()}
                                                    </H1>
                                                    <br />
                                                </Col>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>BNB to ETH</h4>
                                                    <H1>{(Number(stats.bnb_rune)).toLocaleString()}</H1>
                                                    <br />
                                                </Col>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>ETH to BNB</h4>
                                                    <H1>{(Number(stats.eth_rune)).toLocaleString()}</H1>
                                                    <br />
                                                </Col>

                                            </Row>

                                        </Col>

                                        <Col xs={24} sm={24} lg={24} style={{ marginTop: 40 }}>
                                            <strong>TRANSACTIONS</strong>
                                            <br />
                                            <Text>The transactions performed over the bridge.</Text>
                                            <br /><br />
                                            <Row>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>TOTAL</h4>
                                                    <H1>
                                                        {stats.total}
                                                    </H1>
                                                    <br />
                                                </Col>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>BNB to ETH</h4>
                                                    <H1>{stats.bnb_tx}</H1>
                                                    <br />
                                                </Col>
                                                <Col xs={24} sm={8}>
                                                    <h4 style={{ color: "#848E9C" }}>ETH to BNB</h4>
                                                    <H1>{stats.eth_tx}</H1>
                                                    <br />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={24} sm={1} md={3} lg={3}>
                                </Col>

                            </Row>
                        </Row>
                        }


                    </>
                }
            </div>
        </div >
    )
}


export default Home
