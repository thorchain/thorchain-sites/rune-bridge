import React, { useState, useEffect } from "react"

import { Icon as AntIcon, Row, Col, Spin } from 'antd';

import Web3 from 'web3'

import { shortenAddr, getAddrETHLink } from '../../utils/utility'
import { getPoolData } from "../../clients/web3"

import { H1, Text, Button, Center } from '../Components'


const Pools = (props) => {

    const ETH_POOL = {
        'address': '0xcc39592f5cb193a70f262aa301f54db1d600e6da',
        'ticker': 'ETH',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/ETH/0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=ETH&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const USDT_POOL = {
        'address': '0xd279f1351d2828d559bc7a6b82c322c4d5665bed',
        'ticker': 'USDT',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0xdAC17F958D2ee523a2206206994597C13D831ec7`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0xdAC17F958D2ee523a2206206994597C13D831ec7&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const SUSHI_POOL = {
        'address': '0x513d5f1df52eb1b70ffe6b89f3685920fc6c5d89',
        'ticker': 'SUSHI',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0x6b3595068778dd592e39a122f4f5a5cf09c90fe2`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0x6b3595068778dd592e39a122f4f5a5cf09c90fe2&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const SNX_POOL = {
        'address': '0xd535dbf27942551a98cd0723552bdaf70628dbf8',
        'ticker': 'SNX',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0xd535dbf27942551a98cd0723552bdaf70628dbf8`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0xd535dbf27942551a98cd0723552bdaf70628dbf8&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const AAVE_POOL = {
        'address': '0x5285d4ea883dbfb753487e1881db6457c4c227e5',
        'ticker': 'AAVE',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0x5285d4ea883dbfb753487e1881db6457c4c227e5`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0x5285d4ea883dbfb753487e1881db6457c4c227e5&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const YFI_POOL = {
        'address': '0xbd68d831e04479fb8293b862c3bb091a4dcdad3f',
        'ticker': 'YFI',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0xbd68d831e04479fb8293b862c3bb091a4dcdad3f`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0xbd68d831e04479fb8293b862c3bb091a4dcdad3f&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const ALPHA_POOL = {
        'address': '0x72a6d269c3e16239c24d905ca61254ec26523b2d',
        'ticker': 'ALPHA',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0x72a6d269c3e16239c24d905ca61254ec26523b2d`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0x72a6d269c3e16239c24d905ca61254ec26523b2d&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const CREAM_POOL = {
        'address': '0x886fdfcea615e30f7148578ea5f496dbb29a7e19',
        'ticker': 'CREAM',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0x886fdfcea615e30f7148578ea5f496dbb29a7e19`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0x886fdfcea615e30f7148578ea5f496dbb29a7e19&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }
    const PERP_POOL = {
        'address': '0x3dc7722af12100c4f12b3ba1e3c28fd2264808ef',
        'ticker': 'PERP',
        'addLink': `https://exchange.sushiswapclassic.org/#/add/0x3155BA85D5F96b2d030a4966AF206230e46849cb/0x3dc7722af12100c4f12b3ba1e3c28fd2264808ef`,
        'tradeLink': `https://exchange.sushiswapclassic.org/#/swap?inputCurrency=0x3dc7722af12100c4f12b3ba1e3c28fd2264808ef&outputCurrency=0x3155BA85D5F96b2d030a4966AF206230e46849cb`,
    }

    const homeStyles = {
        marginLeft: 0,
        marginTop: 40,
        backgroundColor: "#101921"
    }

    return (
        <>
            <Row style={{}}>
                <Col xs={24} sm={1} md={2} lg={3}>
                </Col>

                <Col xs={24} sm={22} md={20} lg={18} style={homeStyles}>

                    <H1>RUNE Sushiswap Pools</H1>
                    <br></br><br></br>
                    <h4 style={{ color: "#848E9C" }}>
                        <span>Earn fees and incentives in the following Pools. This will help prepare deep liquidity for THORChain ahead of the launch of Ethereum.</span>
                    </h4><br></br>

                    <Row>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={ETH_POOL} />
                        </Col>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={USDT_POOL} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={SUSHI_POOL} />
                        </Col>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={SNX_POOL} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={AAVE_POOL} />
                        </Col>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={YFI_POOL} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={ALPHA_POOL} />
                        </Col>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={CREAM_POOL} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                            <PoolCard pool={PERP_POOL} />
                        </Col>
                        <Col xs={24} lg={12} style={{ padding: 20 }}>
                        </Col>
                    </Row>

                </Col>

                <Col xs={24} sm={1} md={2} lg={3}>
                </Col>

            </Row>
        </>
    )
}

export default Pools


const PoolCard = (props) => {

    const [loaded, setLoaded] = useState(false)
    const [poolData, setPoolData] = useState({
        "runeBal": 1,
        "assetBal": 1,
        "assetSymbol": "TKN",
        "assetAddress": "0x0"
    })

    useEffect(() => {
        getReserves()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getReserves = async () => {
        let poolData = await getPoolData(props.pool.address)
        if (poolData.assetSymbol === 'USDT') {
            poolData.assetBal = poolData.assetBal + '000000000000'
        }
        console.log({ poolData })
        setPoolData(poolData)
        setLoaded(true)
    }

    const getValue = (amount) => {
        // console.log(amount)
        return (+Web3.utils.fromWei(amount, 'ether')).toLocaleString()
    }

    const paneStyle = {
        backgroundColor: "#2e3e4f",
        borderRadius: 20,
        padding: 20,
        margin: 10,
    }

    return (
        <>
            <Row style={{}}>

                <Col xs={24} style={paneStyle}>
                    <H1>{`RUNE-${props.pool.ticker}`}</H1>
                    <br /><br />

                    {!loaded &&
                        <Center>
                            <Spin size="large"></Spin>
                        </Center>
                    }

                    {loaded &&
                        <>
                            <Row>
                                <Col xs={12}>
                                    <Center><Text size={18}>RUNE</Text></Center>
                                    <Center><Text size={32}>{getValue(poolData.runeBal)}</Text></Center>
                                </Col>
                                <Col xs={12}>
                                    <Center><Text size={18}>{poolData.assetSymbol}</Text></Center>
                                    <Center><Text size={32}>{getValue(poolData.assetBal)}</Text></Center>
                                </Col>
                            </Row>

                        </>
                    }
                    <br />
                    <Row>
                        <Col xs={12}>
                            <Center><Button><a style={{ color: "#FFFFFF" }} href={`${props.pool.addLink}`} target="blank">ADD LIQUIDITY</a><AntIcon type="arrow-right" /></Button></Center>
                        </Col>
                        <Col xs={12}>
                            <Center><Button><a style={{ color: "#FFFFFF" }} href={`${props.pool.tradeLink}`} target="blank">TRADE</a><AntIcon type="arrow-right" /></Button></Center>
                        </Col>
                    </Row>
                    <br />
                    <a href={`${getAddrETHLink(props.pool.address)}`} target="blank">{shortenAddr(props.pool.address)}</a>
                </Col>
            </Row>
        </>
    )
}