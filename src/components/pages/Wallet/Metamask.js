import React, { useState, useContext } from 'react'
import { Row, Col, Icon as AntIcon } from 'antd'
import { Link } from "react-router-dom"

import { Context } from '../../../context'
import { Button, Text } from '../../Components'

import Web3 from 'web3'

const Metamask = page => {

  const context = useContext(Context)

  const pause = (ms) => new Promise(resolve => setTimeout(resolve, ms))

  const [contLoad, setContLoad] = useState(false)
  const [loaded, setLoaded] = useState(false)

  const connect = async (props) => {
    if (contLoad === false) {
      window.web3 = new Web3(window.ethereum)
      if (window.web3._provider) {
        context.setContext({ 'web3Wallet': true })
        const account = (await window.web3.eth.getAccounts())[0]
        console.log({ account })
        if (account) {
          setContLoad(false)

          context.setContext({
            "wallet": {
              "address": account,
            }
          }
            // , () => {page.history.push("/switch")}
          )
          setLoaded(true)
        } else {
          await enableMetaMask(props)
          setContLoad(true)
          await pause(3000)
          connect(props)
        }
      }
      else {
        context.setContext({ 'web3Wallet': false })
      }
    }
  }

  const enableMetaMask = async () => {
    //console.log('connecting')
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      window.ethereum.enable()
      //await connectWallet()
      return true;
    }
    return false;
  }

  return (
    <div>
      <Row style={{ bottom: 5 }}>
        <Text size={18}>Click to connect with Metamask.</Text>
      </Row>

      <Row>
        <Col span={12}>

          <Button
            onClick={() => connect()}
            fill={false}
            style={{ marginTop: 24, marginLeft: 0 }}
          >
            Connect
                {/* <AntIcon type="arrow-right" /> */}

          </Button>
        </Col>

      </Row>
      <Row style={{ marginBottom: 10 }}>
        {loaded &&
          <Link to="/switch">
            <Button
              disabled={loaded === false}
              style={{ float: "right" }}
              fill={true}
            >
              CONTINUE
          <AntIcon type="arrow-right" />
            </Button>
          </Link>
        }

      </Row>
    </div>
  )
}

export default Metamask
