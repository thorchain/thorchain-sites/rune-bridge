import React, { useEffect, useState, useContext } from 'react'
import { Context } from '../../../context'

import Web3 from 'web3'

import Binance from "../../../clients/binance"
import { SYMBOL } from '../../../env'
import { getETHBalance, getRUNEBalance } from "../../../clients/web3"

import { Row, Col } from 'antd'
import { H1, Text, Button, Coin } from "../../Components"

const Wallet = props => {
  const context = useContext(Context)

  const [loadingBalances, setLoadingBalances] = useState(false)
  const [balances, setBalances] = useState(null)

  useEffect(() => {
    getBalances()
    // eslint-disable-next-line
  }, [context.wallet])

  const getBalances = async () => {
    if (context.wallet && context.wallet.address) {
      setLoadingBalances(true)

      if (!context.web3Wallet) {
        try {
          let response = await Binance.getBalances(context.wallet.address)
          //console.log("Balances:", response)

          const b = (response || []).map((bal) => (
            {
              "icon": bal.symbol === SYMBOL ? "coin-rune" : "coin-bep",
              "ticker": bal.symbol,
              "free": parseFloat(bal.free),
              "frozen": parseFloat(bal.frozen),
              "locked": parseFloat(bal.locked),
            }
          ))
          let relevantBalances = b.filter((x) => x.ticker === SYMBOL || x.ticker === 'BNB')

          //console.log(relevantBalances)
          setBalances([...relevantBalances])
          setLoadingBalances(false)
        } catch (error) {
          setLoadingBalances(false)
        }

      } else {

        let rune_bal = await getRUNEBalance(context.wallet.address)
        let eth_bal = await getETHBalance(context.wallet.address)

        let runeBalance = {
          "icon": "coin-rune",
          "ticker": SYMBOL,
          "free": Number(Web3.utils.fromWei(rune_bal)),
          "frozen": 0,
          "locked": 0
        }

        let ethBalance = {
          "icon": "coin-bep",
          "ticker": 'ETH',
          "free": Number(Web3.utils.fromWei(eth_bal)),
          "frozen": 0,
          "locked": 0
        }

        let relevantBalances = [runeBalance, ethBalance]
        //console.log(relevantBalances)
        setBalances([...relevantBalances])

        setLoadingBalances(false)

      }

      // Binance.getBalances(context.wallet.address)
      //   .then((response) => {
      //     const b = (response || []).map((bal) => (
      //       {
      //         "icon": bal.symbol === "RUNE-B1A" ? "coin-rune": "coin-bep",
      //         "ticker": bal.symbol,
      //         "free": parseFloat(bal.free),
      //         "frozen": parseFloat(bal.frozen),
      //         "locked": parseFloat(bal.locked),
      //       }
      //     ))
      //     setBalances([...b])
      //     setLoadingBalances(false)
      //   })
      //   .catch((error) => {
      //     setLoadingBalances(false)
      //   })

    } else {
      props.history.push("/wallet/unlock")
    }
  }

  const forgetWallet = () => {
    context.forgetWallet()
    props.history.push("/")
  }


  const rowStyle = {margin: "10px 0px"}

  return (
    <div style={{margin: 10}}>
      <H1>Wallet</H1>
      <Row style={rowStyle}>
        <Text size={18} bold>Balances</Text>
      </Row>

      {!loadingBalances && (balances || []).map((coin) => (
        <Row key={coin.ticker} style={rowStyle}>
          <Col xs={24} sm={12} md={6}>
            <Coin {...coin} />
          </Col>
        </Row>
      ))
      }

      <Row style={rowStyle}>
        <div style={{marginTop: 20}}>
          <Button onClick={forgetWallet}>Forget Wallet</Button>
        </div>
      </Row>

    </div>
  )
}

export default Wallet
