import React, { useContext } from 'react'
import { Row, Col, Tabs } from 'antd'

import Breakpoint from 'react-socks';

import { Center, H1, Text } from '../../Components'
import Mnemonic from "./Mnemonic"
import Metamask from "./Metamask"
import Keystore from "./Keystore"
import Ledger from "./Ledger"
import WalletConnectPane from "./walletConnect"
import { Context } from '../../../context'

const { TabPane } = Tabs;

const Unlock = (props) => {

  const context = useContext(Context)

  return (
    <Row style={{ marginBottom: "200" }}>

      <Col xs={24} sm={1} md={2}>
      </Col>

      <Col xs={24} sm={22} md={20}>

        <div>
          <Row>
            <br/>
          <Center><H1>Unlock Your {context.chain} Wallet</H1></Center>
          </Row>
          
          <Row>
            <Center>
            <br></br>
              <Text color='#EE5366'><i><b>Important!</b></i></Text>
            </Center>
          </Row>
          
          <Row>
            <Center>
            <Text color='#EE5366'>Please check that you are visiting <i>https://www.runebridge.org</i>
            </Text>
            </Center>
          </Row>
          <br></br>
        </div>

        <Row style={{ marginBottom: "200" }}>
          <Col>
            <p>
              <Text size={16} bold>Select how you would like to unlock</Text>
            </p>

            {context.chain !== 'ETH' &&
              <>
                <Breakpoint small down>
                  <Tabs defaultActiveKey="1" tabPosition={"top"}>
                    <TabPane tab={<Text size={16}><i>WalletConnect</i></Text>} key="1">
                      <WalletConnectPane {...props} />
                    </TabPane>

                    <TabPane tab={<Text size={16}><i>Ledger Device</i></Text>} key="2">
                      <Ledger {...props} />
                    </TabPane>

                    <TabPane tab={<Text size={16}><i>Keystore File</i></Text>} key="3">
                      <Keystore {...props} />
                    </TabPane>
                    <TabPane tab={<Text size={16}><i>Mnemonic Phrase</i></Text>} key="4">
                      <Mnemonic {...props} />
                    </TabPane>
                  </Tabs>
                </Breakpoint>

                <Breakpoint medium up>
                  <Tabs defaultActiveKey="3" tabPosition={"left"}>
                    <TabPane tab={<Text size={16}><i>WalletConnect</i></Text>} key="1">
                      <WalletConnectPane {...props} />
                    </TabPane>

                    <TabPane tab={<Text size={16}><i>Ledger Device</i></Text>} key="2">
                      <Ledger {...props} />
                    </TabPane>

                    <TabPane tab={<Text size={16}><i>Keystore File</i></Text>} key="3">
                      <Keystore {...props} />
                    </TabPane>
                    <TabPane tab={<Text size={16}><i>Mnemonic Phrase</i></Text>} key="4">
                      <Mnemonic {...props} />
                    </TabPane>
                  </Tabs>
                </Breakpoint>
              </>

            }

            {context.chain === 'ETH' &&
              <>
                <Tabs defaultActiveKey="2" tabPosition={"left"}>
                  <TabPane tab={<Text size={16}><i>WalletConnect</i></Text>} key="1">
                    <WalletConnectPane {...props} />
                  </TabPane>

                  <TabPane tab={<Text size={16}><i>Metamask</i></Text>} key="2">
                    <Metamask {...props} />
                  </TabPane>
                </Tabs>
              </>
            }


          </Col>
        </Row>
      </Col>

      <Col xs={24} sm={1} md={2}>
      </Col>

    </Row>
  )
}

export default Unlock
