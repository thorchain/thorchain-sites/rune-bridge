import Web3 from 'web3'

import ERC20_ABI from '../artifacts/ERC20.json'
import BRIDGE_ABI from '../artifacts/Bridge.json'
import POOL_ABI from '../artifacts/POOL.json'
import { ETH_RUNE_ADDR, INFURA_API } from '../env'

export const isAddressETH = (address) => {
    return Web3.utils.isAddress(address)
}

export const getWeb3 = () => {
    return new Web3(Web3.givenProvider || "http://localhost:7545")
}
export const getRemoteWeb3 = () => {
    return new Web3(new Web3.providers.HttpProvider(INFURA_API))
}

export const getETHBalance = async (address) => {
    return await getRemoteWeb3().eth.getBalance(address)
}

export const getTokenContract = (address) => {
    var web3 = getRemoteWeb3()
    return new web3.eth.Contract(ERC20_ABI, address)
}

export const getPoolContract = (address) => {
    var web3 = getRemoteWeb3()
    return new web3.eth.Contract(POOL_ABI, address)
}

export const getBridgeContract = (address) => {
    var web3 = getWeb3()
    return new web3.eth.Contract(BRIDGE_ABI, address)
}

export const getTokenSupply = async () => {
    var contractToken = getTokenContract(ETH_RUNE_ADDR)
    return await contractToken.methods.totalSupply().call()
}

export const getRUNEBalance = async (address) => {
    var contractToken = getTokenContract(ETH_RUNE_ADDR)
    return await contractToken.methods.balanceOf(address).call()
}

export const depositRune = async (amount, destination, account, address) => {
    var contract = getBridgeContract(address)
    let value = Web3.utils.toWei(amount, 'ether')
    console.log(value, destination, account, address)
    let tx = await contract.methods.deposit(value, `SWITCH:${destination}`).send({from:account, gasLimit:50000})
    return tx.transactionHash
}

// export const getPoolSupply = async (address) => {
//     var contract = getPoolContract(address)
//     return await contract.methods.getReserves().call()
// }
export const getPoolData = async (address) => {
    let token0, reserves, runeBal, assetBal, assetSymbol, assetAddress;
    var contract = getPoolContract(address)
    reserves = await contract.methods.getReserves().call()
    token0 = await contract.methods.token0().call()
    if(token0 === ETH_RUNE_ADDR){
        runeBal = reserves[0]
        assetBal = reserves[1]
        assetAddress = await contract.methods.token1().call()
        assetSymbol = await getTokenContract(assetAddress).methods.symbol().call()
    } else {
        runeBal = reserves[1]
        assetBal = reserves[0]
        assetAddress = await contract.methods.token0().call()
        assetSymbol = await getTokenContract(assetAddress).methods.symbol().call()
    }
    let poolData =  {
        "runeBal":runeBal,
        "assetBal":assetBal,
        "assetSymbol":assetSymbol,
        "assetAddress":assetAddress
    }
    return poolData
}