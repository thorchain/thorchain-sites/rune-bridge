import bnbClient from '@binance-chain/javascript-sdk'
import TokenManagement from '@binance-chain/javascript-sdk'
import axios from 'axios'

import { NET, isTestnet } from '../env'

class Binance {
  constructor() {
    this.baseURL = "https://dex.binance.org"
    this.explorerBaseURL = "https://explorer.binance.org"
    if (isTestnet) {
      this.baseURL = "https://testnet-dex.binance.org"
      this.explorerBaseURL = "https://testnet-explorer.binance.org"
    }

    this.net = NET
    //console.log("Net:", this.net)

    this.httpClient = axios.create({
      baseURL: this.baseURL + "/api/v1",
      contentType: "application/json",
    })

    this.bnbClient = new bnbClient(this.baseURL);
    this.bnbClient.chooseNetwork(this.net)
    this.bnbClient.initChain()
    this.bnbTokens = new TokenManagement(this.bnbClient).tokens;
  }

  setPrivateKey(privateKey) {
    this.bnbClient.setPrivateKey(privateKey)
    this.bnbClient.chooseNetwork(this.net)
    this.bnbClient.initChain()
  }

  useLedgerSigningDelegate(ledgerApp, preSignCb, postSignCb, errCb, hdPath) {
    return this.bnbClient.useLedgerSigningDelegate(ledgerApp, preSignCb, postSignCb, errCb, hdPath)
  }

  clearPrivateKey() {
    this.bnbClient.privateKey = null
  }

  getPrefix() {
    return isTestnet ? "tbnb" : "bnb"
  }

  txURL(tx) {
    return this.explorerBaseURL + "/tx/" + tx
  }

  fees() {
    return this.httpClient.get("/fees")
  }

  async price(symbol) {
    const response = await axios.get("https://api.coingecko.com/api/v3/simple/price?ids=thorchain&vs_currencies=usd")
    return response.data.thorchain.usd ? response.data.thorchain.usd : null
  }

  // convert fee number into BNB tokens
  calculateFee(x) {
    return x / 100000000
  }

  getBalances(address) {
    return this.bnbClient.getBalance(address)
  }

  getAccount(address) {
    return this.bnbClient.getAccount(address)
  }

  getMarkets(limit = 1000, offset = 0) {
    return this.bnbClient.getMarkets(limit, offset)
  }

  async multiSend(address, transactions, memo = "") {
    // send coins!
    const result = await this.bnbClient.multiSend(address, transactions, memo)

    // clear private key from memory after each transaction
    this.clearPrivateKey()

    return result
  }
}

var binance = window.binance = new Binance()
export default binance

export const isAddressBNB = async (address) => {
  // let addressValid = await validAddress(address)
  // if (!addressValid) {
  //   console.log('false address')
  //   return false
  // }
  if (address.length === 43 && address.substring(0, 4) === "tbnb" && isTestnet) {
    return true
  } else if (address.length === 42 && address.substring(0, 3) === "bnb" && !isTestnet) {
    return true
  } else {
    return false
  }
}

export const notExchangeBNB = (address) => {
  let exchangeArray = ['bnb136ns6lfw4zs5hg4n85vdthaad7hq5m4gtkgf23',
    'bnb1jxfh2g85q3v0tdq56fnevx6xcxtcnhtsmcu64m',
    'bnb1590q74v4ywtlnv0y0t04eaqtlz5ksw06r85s5m',
    'bnb1u2agwjat20494fmc6jnuau0ls937cfjn4pjwtn',
    'bnb142q467df6jun6rt5u2ar58sp47hm5f9wvz2cvg',
    'bnb1j725qk29cv4kwpers4addy9x93ukhw7czfkjaj',
    'bnb1v8vkkymvhe2sf7gd2092ujc6hweta38xadu2pj',
    'bnb122tk7vsy4sz2dfn4g78tm4z6p8jan0tu5q4npp',
    'bnb1jvqy75h4yr3xv2rzstm0zulkp50sjy5q3qmy76',
    'bnb1ultyhpw2p2ktvr68swz56570lgj2rdsadq3ym2',
    'bnb1skl4n4vrzx3ty9ujaut8rmkhkmtl4t04ysllfm',
    'bnb1ag3rpe9lten7fhyqg4cde9qusrv3dv67lsshup']
  if (exchangeArray.includes(address)) {
    return false
  } else {
    return true
  }
}

/* Address validation */
// bnb136ns6lfw4zs5hg4n85vdthaad7hq5m4gtkgf23
// bnb1t6tnm2rckd3pfptngj6u8466v3ah4fcdu78n5y
// const validAddress = async (address) => {
//   console.log(address)
//   let binanceClient = new bnbClient("https://dex.binance.org");
//   binanceClient.chooseNetwork(NET)
//   binanceClient.initChain()
//   let result = await binanceClient.getAccount(address)
//   console.log(result)
//   if (!result){
//     return true
//   }
//   if (result.result.flags > 0) {
//     console.log('return false')
//     return false
//   } else {
//     console.log('return true')
//     return true
//   }
// }
