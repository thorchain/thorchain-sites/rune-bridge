import { crypto } from '@binance-chain/javascript-sdk'
import base64js from 'base64-js'

import Binance from "./binance"
import {depositRune} from "./web3"
import { CHAIN_ID, SYMBOL } from '../env'

const NETWORK_ID = 714
const SWITCH_MEMO = 'SWITCH'

export const handleTx = async (toAddress, values, context) => {
    if (context.wallet.walletconnect) {
        handleMobileSign(toAddress, values, context)
    } else if (context.web3Wallet) {
        handleMetamaskSign(toAddress, values, context)
    } else {
        handleKeySign(toAddress, values, context)
    }
}

const handleMobileSign = async (toAddress, values, context) => {
    try {
        let response = await Binance.getAccount(context.wallet.address)
        const account = response.result
        console.log("AccountInfo:", account)
        const tx = window.tx = {
            accountNumber: account.account_number.toString(),
            chainId: CHAIN_ID,
            sequence: account.sequence.toString(),
            memo: `${SWITCH_MEMO}:${values.address}`
        };

        tx.send_order = {
            inputs: {
                "address": base64js.fromByteArray(crypto.decodeAddress(context.wallet.address)),
                "coins": {
                    "denom": SYMBOL,
                    "amount": +values.amount*100000000,
                }
            },
            outputs: {
                "address": base64js.fromByteArray(crypto.decodeAddress(toAddress)),
                "coins": {
                    "denom": SYMBOL,
                    "amount": +values.amount*100000000,
                }
            }
        }

        window.mywall = context.wallet.walletconnect
        try {
            let result = await context.wallet.walletconnect.trustSignTransaction(NETWORK_ID, tx)
            window.result = result
            console.log("Successfully signed tx:", result);
            try {
                let response = await Binance.bnbClient.sendRawTransaction(result, true)
                console.log("Response", response)
                return response
            } catch (error) {
                console.error(error)
                return error
            }

        } catch (error) {
            console.error(error);
            return error
        }
    } catch (error) {
        window.err = error
        console.error(error)
        return error
    }
}

const handleKeySign = async (toAddress, values, context) => {

    if (context.wallet.keystore) {
        try {
            const privateKey = crypto.getPrivateKeyFromKeyStore(
                context.wallet.keystore,
                values.password
            )
            Binance.setPrivateKey(privateKey)

        } catch (error) {
            window.error = error
            console.error("Validating keystore error:", error)
            return error
        }

    } else if (context.wallet.ledger) {
        Binance.useLedgerSigningDelegate(
            context.wallet.ledger,
            null, null, null,
            context.wallet.hdPath,
        )
    } else {
        throw new Error("no wallet detected")
    }

    try {
        let response = await Binance.bnbClient.transfer(
            context.wallet.address,
            toAddress,
            values.amount,
            SYMBOL,
            `${SWITCH_MEMO}:${values.address}`)

        if (response.result[0].ok) {
            return response
        }
    } catch (error) {
        window.error = error
        return error
    }

}

const handleMetamaskSign = async (contract, values, context) => {

    try {
        let tx = depositRune(values.amount, values.address, context.wallet.address, contract)
        return tx
    } catch (error) {
        window.error = error
        console.error("Validating keystore error:", error)
        return error
    }
}