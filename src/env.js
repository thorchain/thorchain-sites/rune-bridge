const { REACT_APP_INFURA_API } = process.env;

const isMainnet = true
const isTestnet = !isMainnet

const NET = isTestnet ? "KOVAN" : "MAINNET"
const CHAIN_ID = isTestnet ? "Binance-Chain-Nile" : "Binance-Chain-Tigris"
const SYMBOL = isTestnet ? "RUNE-67C" :  "RUNE-B1A"
const DEX_URL = isTestnet ? "https://testnet-explorer.binance.org" : "https://explorer.binance.org"
const ETHERSCAN_URL = isTestnet ? "https://kovan.etherscan.io" : "https://etherscan.io"

const BNB_CONTROLLER_ADDR = isTestnet ? 'tbnb1vnl4sv4vjn2xw7su88jqdyf2fe2hz2rdd5v437' : 'bnb1e4q8whcufp6d72w8nwmpuhxd96r4n0fstegyuy'
const ETH_CONTROLLER_ADDR = isTestnet ? '0x3efF38C0e1e5DD6Bd58d3fa79cAecc4Da46C8866' : '0x3eff38c0e1e5dd6bd58d3fa79caecc4da46c8866'
const BASE_URL = isTestnet ? 'https://runebridge.herokuapp.com' : 'https://runebridgeapi.herokuapp.com'

const ETH_RUNE_ADDR = isTestnet ? '0xDd3bc2929baFc11298A74a6daCE3A32E5b628c70' : '0x3155BA85D5F96b2d030a4966AF206230e46849cb'


// const REACT_APP_INFURA_API=`99f293d58d554e0cb7a4f1fe9737716c`
const INFURA_API = isTestnet ? 'https://kovan.infura.io/v3/' + REACT_APP_INFURA_API : 'https://mainnet.infura.io/v3/' + REACT_APP_INFURA_API

export {
    NET,
    CHAIN_ID,
    SYMBOL,
    isTestnet,
    isMainnet,
    DEX_URL,
    ETHERSCAN_URL,
    BASE_URL,
    BNB_CONTROLLER_ADDR,
    ETH_CONTROLLER_ADDR,
    INFURA_API, 
    ETH_RUNE_ADDR
}
