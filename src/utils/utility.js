import { DEX_URL, ETHERSCAN_URL } from '../env'


// replacement for .toLocaleString(). That func does rounding that I don't like.
const AmounttoString = (amount) => {
  // Converting to string with rounding to 8 digits
  var parts = amount.toPrecision(8).replace(/\.?0+$/, '').split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

const StringToAmount = (string) => {
  // Converting from string
  const number = string.replace(/,/g, "");
  const final = Number(number).toFixed(2)
  return final;
}

function formatDate(lastUpdatedDate) {
  return lastUpdatedDate.toLocaleDateString('en-GB', {
    day: 'numeric', month: 'long', year: 'numeric'
  }).replace(/ /g, ' ');
}

const shortenAddr = (address) => {
  if (address) {
    const addr = address.substring(0, 7).concat('...')
    const addrShort = addr.concat(address.substring(address.length - 4, address.length))
    return addrShort
  } else {
    return 'pending'
  }

}

const shortenHash = (hash) => {
  if (hash) {
    const start = hash.substring(0, 4).concat('...')
    const hashShort = start.concat(hash.substring(hash.length - 4, hash.length))
    return hashShort
  } else {
    return 'pending'
  }
}

const getAddrETHLink = (address) => {
  return `${ETHERSCAN_URL}/address/${address}`
}
const getAddrBNBLink = (address) => {
  return `${DEX_URL}/address/${address}`
}
const getTxETHLink = (tx) => {
  return `${ETHERSCAN_URL}/tx/${tx}`
}
const getTxBNBLink = (tx) => {
  return `${DEX_URL}/tx/${tx}`
}

export {
  AmounttoString, StringToAmount,
  formatDate, shortenAddr, shortenHash,
  getAddrETHLink, getAddrBNBLink, getTxETHLink, getTxBNBLink
}
